import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab } from 'react-bootstrap'
import MonthlyChart from '../components/MonthlyChart'

export default function insights(){

	const [distances, setDistances] = useState([])
	const [expenditures, setExpenditures] = useState([])
	const [durations, setDurations] = useState([])

	useEffect(() => {
		//getting the data
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.travels.length > 0){
				let monthlyDistance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyDurations = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyExpenditures = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

				data.travels.forEach(travel => {
					//retrieve index of the month
					//January = 0, Feb = 1 ...
					const index = moment(travel.date).month()


					monthlyDistance[index] += (travel.distance/1000)//compute by kilometers
					//monthlyDistance[index] = monthlyDistance[index] + (travel.distance/1000)
					monthlyDurations[index] += (parseInt(travel.duration)/60)
					monthlyExpenditures[index] += (travel.charge.amount)

				})

				setDistances(monthlyDistance)
				console.log(monthlyDistance)

				setDurations(monthlyDurations)
				console.log(monthlyDurations)

				setExpenditures(monthlyExpenditures)
				console.log(monthlyExpenditures)
			}
		})
	}, [])

		

	return(
		<Tabs defaultActiveKey="distances" id="monthlyFigures">
			<Tab eventKey="distances" title="Monthly Distance Travelled">
				<MonthlyChart figuresArray={distances} label="Monthly total in kilometers" />
			</Tab>
				<Tab eventKey="durations" title="Monthly Durations Travelled">
				<MonthlyChart figuresArray={sale_date} label="Monthly total in minutes" />
			</Tab>
			<Tab eventKey="expenditures" title="Monthly Expenditures Travelled">
				<MonthlyChart figuresArray={expenditures} label="Monthly total in Philippine Peso" />
			</Tab>
		</Tabs>
	)
}